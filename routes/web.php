<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Auth::routes();
Auth::routes(['verify' => true]);

/*
Route::get('protege', function () {
    return 'affichage de la route protégé';
})->middleware('verified');
*/
//Route::get('/', 'AdController@home')->name('home')->middleware('verified');
Route::get('/', 'AdController@home')->name('home');


Route::get('a-faire', 'AdController@doPage')->name('dopage');

// Authentication Routes...
Route::prefix('connexion')->group(function () {
    Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/', 'Auth\LoginController@login');
});
Route::post('deconnexion', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::prefix('enregistrement')->group(function () {
    Route::get('/', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/', 'Auth\RegisterController@register');
});
// Password Reset Routes...
Route::prefix('passe')->group(function () {
    Route::get('change', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('change/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('change', 'Auth\ResetPasswordController@reset')->name('password.update');
});



//
//Route::get('{num}', 'NewsController@show')->name('news.show');

//Route::get('news/{num}/edit', 'NewsController@show')->name('news.edit');



//Route::prefix('news')->group(function () {
    //Route::get('news', 'NewsController@index')->name('news.index');
//});



Route::resource('news', 'NewsController')
//    ->middleware('verified')
    ->parameters(['news' => 'news'    ]);   // pour edit

Route::prefix('news')->group(function () {

    Route::get('destroy/{news}', 'NewsController@destroy')->name('news.destroy');
  //  Route::post('change', 'NewsController@update')->name('news.update');

    /*
    Route::get('{num}', 'NewsController@show')->name('news.show');
    Route::get('/', 'NewsController@index')->name('news.index');
    Route::get('{num}/modif', 'NewsController@show')->name('news.modif');
*/
});


Route::resource('annonces', 'AdController')
    ->parameters(['annonces' => 'ad'    ])   // pour edit
    ->except(['index', 'destroy']);


Route::prefix('annonces')->group(function () {

    // affiche une annonce
    Route::get('{ad}/voir', 'AdController@show')->name('annonces.show');

    // affiche la liste
    Route::get('{region?}/{departement?}/{commune?}', 'AdController@index')->name('annonces.index');
    Route::get('{region?}/{departement?}/{commune?}/{cate?}', 'AdController@index')->name('annonces.index');

    // formulaire de recherche
    Route::post('recherche', 'AdController@search')->name('annonces.search')->middleware('ajax');
});


// message
Route::middleware('ajax')->group(function () {
    Route::post('images-save', 'UploadImagesController@store')->name('save-images');
    Route::delete('images-delete', 'UploadImagesController@destroy')->name('destroy-images');
    Route::get('images-server','UploadImagesController@getServerImages')->name('server-images');

    Route::post('message', 'UserController@message')->name('message');
});

// admin
Route::prefix('admin')->middleware('admin')->group(function () {
    Route::get('/', 'AdminController@index')->name('admin.index');

    Route::prefix('annonces')->group(function () {
        Route::get('/', 'AdminController@ads')->name('admin.ads');
        Route::middleware('ajax')->group(function () {
            Route::post('approve/{ad}', 'AdminController@approve')->name('admin.approve');
            Route::post('refuse', 'AdminController@refuse')->name('admin.refuse');
        });

        Route::get('obsoletes', 'AdminController@obsoletes')->name('admin.obsoletes');
    });

    Route::prefix('messages')->group(function () {
        Route::get('/', 'AdminController@messages')->name('admin.messages');
        Route::post('approve/{message}', 'AdminController@messageApprove')->name('admin.message.approve');
        Route::post('refuse', 'AdminController@messageRefuse')->name('admin.message.refuse');
    });

});

Route::prefix('admin/annonces')->group(function () {
    Route::middleware('ajax')->group(function () {
        Route::post('addweek/{ad}', 'AdminController@addWeek')->name('admin.addweek');
        Route::delete('destroy/{ad}', 'AdminController@destroy')->name('admin.destroy');
    });
});


// user
Route::prefix('utilisateur')->middleware('user')->group(function () {
    Route::get('/', 'UserController@index')->name('user.index')->middleware('verified');
    Route::prefix('annonces')->group(function () {
        Route::get('actives', 'UserController@actives')->name('user.actives');
        Route::get('obsoletes', 'UserController@obsoletes')->name('user.obsoletes');
        Route::get('attente', 'UserController@attente')->name('user.attente');
    });
    Route::prefix('profil')->group(function () {
        Route::get('email', 'UserController@emailEdit')->name('user.email.edit');
        Route::put('email', 'UserController@emailUpdate')->name('user.email.update');
        Route::get('donnees', 'UserController@data')->name('user.data');
        Route::get('abonnement', 'SubscriptionController@show')->name('user.subscription.select');
        Route::get('paiement', 'SubscriptionController@index')->name('user.subscription.create');
        Route::post('paiement', 'SubscriptionController@index')->name('user.subscription.create');
        Route::post('annuler', 'SubscriptionController@cancel')->name('user.subscription.cancel');
    });
});

Route::view('group', 'group')->name('group')->middleware('verified');
Route::view('legal', 'legal')->name('legal');
Route::view('confidentialite', 'confidentialite')->name('confidentialite');
Route::view('information', 'information')->name('information');
Route::view('tarif', 'tarif')->name('tarif');


// chat
Route::get('chat', 'ChatsController@index');
Route::get('chat/{user}', 'ChatsController@index')->name('chat.liste');
Route::get('chats', 'ChatsController@fetchMessages');
Route::post('chats', 'ChatsController@sendMessage');

Route::get('locale/{locale}', function ($locale){
    session()->put('locale', $locale);
    return redirect()->back();
});



Route::get('payment', 'PayPalController@payment')->name('payment');
Route::get('cancel', 'PayPalController@cancel')->name('payment.cancel');
Route::get('payment/success', 'PayPalController@success')->name('payment.success');

/*
// paypal
Route::get('abonnement', 'PaypalController@index')->name('subscription');
Route::get('paypal/express-checkout', 'PaypalController@expressCheckout')->name('paypal.express-checkout');
Route::get('paypal/express-cancel/{invoice_id?}', 'PaypalController@expressCancel')->name('paypal.express-cancel');
Route::get('paypal/express-checkout-success', 'PaypalController@expressCheckoutSuccess');
Route::post('paypal/notify', 'PaypalController@notify');
*/


// Stripe

Route::get('abonnement', 'SubscriptionController@show')->name('abonnement');
Route::post('order-post', ['as'=>'order-post','uses'=>'SubscriptionController@orderPost']);


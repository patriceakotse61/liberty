@extends('layouts.user')
@section('content')

    <video autoplay muted preload="none" style=" width:100%;  height:100%;">
        @if ($mode_children == true )
            <source src="/video/nature.mp4" type="video/mp4">
        @else
            <source src="/video/libert.mp4" type="video/mp4">
        @endif
        Your browser does not support the HTML5 video tag.
    </video>

    @if (Session::has('message'))
        <div class="alert alert-{{ Session::get('code') }}">
            <p>{{ Session::get('message') }}</p>
        </div>
    @endif

    <section class="py-6">
        <div class="container">
            <div class="row">
                @if ($abo)
                    @if($abo['reccuring'] == '')
                        <div class="col-md-4">
                            <!-- pricing card-->
                            <div class="card mb-5 mb-lg-0 border-0 shadow">
                                <div class="card-body">
                                    <h2 class="text-base subtitle text-center text-primary py-3">Visiteur</h2>
                                    <p class="text-muted text-center"><span class="h1 text-dark">1€</span><span class="ml-2">/ mois</span></p>
                                    <hr/>
                                    <ul class="fa-ul my-4">
                                        <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>voir les annonces et répondre</li>
                                        <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>1 Photos</li>
                                        <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>500 caractères</li>
                                        <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Messagerie</li>
                                        <div class="text-center">
                                            <a href="{{ route('paypal.express-cancel', ['invoice_id' => $abo['id']]) }}" class='btn-info btn'>Arrêter votre abonnement</a>
                                        </div>
                                    </ul>

                                </div>
                            </div>
                            <!-- /pricing card -->
                        </div>
                        <div class="col-md-8">
                            <br/><br/><br/>
                            Vous avez un abonnement ({{ $abo['title'] }}) qui se termine le {{ $abo['end'] }}
                        </div>
                    @else
                        {{ $abo['subscription'] }}

                        Vous avez un abonnement récurent ({{ $abo['title'] }}) qui sera relancé le {{ $abo['end'] }}

                        @if($abo['subscription'] == 'QK6A-JI6S-7ETR-0A6C')
                            <div class="col-lg-4">
                                <!-- pricing card-->
                                <div class="card mb-5 mb-lg-0 border-0 shadow">
                                    <div class="card-body">
                                        <h2 class="text-base subtitle text-center text-primary py-3">Bonze</h2>
                                        <p class="text-muted text-center"><span class="h1 text-dark">2€</span><span class="ml-2">/ mois</span></p>
                                        <hr/>
                                        <ul class="fa-ul my-4">
                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>voir les annonces et répondre</li>
                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>1 Photo</li>
                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>800 caractères</li>
                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Messagerie</li>

                                            <div class="text-center">
                                                <a href="{{ route('paypal.express-cancel', ['invoice_id' => $abo['id']]) }}" class='btn-info btn'>Arrêter votre abonnement</a>
                                            </div>

                                        </ul>

                                    </div>
                                </div>
                                <!-- /pricing card -->

                            </div>
                        @endif

                        @if($abo['subscription'] == 'SXFP-CHYK-ONI6-S89U')
                            <div class="col-lg-4">
                                <!-- pricing card-->
                                <div class="card mb-5 mb-lg-0 border-0 shadow">
                                    <div class="card-status bg-primary"></div>
                                    <div class="card-body">
                                        <h2 class="text-base subtitle text-center text-primary py-3">Argent</h2>
                                        <p class="text-muted text-center"><span class="h1 text-dark">4€</span><span class="ml-2">/ mois</span></p>
                                        <hr/>
                                        <ul class="fa-ul my-4">
                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Voir et repondre aux annonces</li>
                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>3 photos</li>
                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>1000 caractères</li>
                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Messagerie</li>


                                        </ul>
                                        <div class="text-center">
                                            <a href="{{ route('paypal.express-cancel', ['invoice_id' => $abo['id']]) }}" class='btn-info btn'>Arrêter votre abonnement</a>
                                        </div>

                                    </div>
                                </div>
                                <!-- /pricing card -->
                            </div>
                        @endif
                        @if($abo['subscription'] == 'XNSS-HSJW-3NGU-8XTJ')
                            <div class="col-lg-4">
                                <!-- pricing card-->
                                <div class="card mb-5 mb-lg-0 border-0 shadow">
                                    <div class="card-body">
                                        <h2 class="text-base subtitle text-center text-primary py-3">Or</h2>
                                        <p class="text-muted text-center"><span class="h1 text-dark">5€</span><span class="ml-2">/ moi</span></p>
                                        <hr/>
                                        <ul class="fa-ul my-4">
                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>2 Annonces</li>

                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Voir et repondre aux annonces</li>
                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>5 photos</li>
                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>2000 caractères</li>
                                            <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Messagerie</li>
                                        </ul>
                                        <div class="text-center">
                                            <a href="{{ route('paypal.express-cancel', ['invoice_id' => $abo['id']]) }}" class='btn-info btn'>Arrêter votre abonnement</a>
                                        </div>

                                    </div>
                                </div>
                                <!-- /pricing card -->
                            </div>
                        @endif


                    @endif
                    <div class="panel panel-default">
                        <div class="panel-body">

                        </div>
                    </div>

                @else
                    <div class="col-lg-4">
                        <!-- pricing card-->
                        <div class="card mb-5 mb-lg-0 border-0 shadow">
                            <div class="card-body">
                                <h2 class="text-base subtitle text-center text-primary py-3">Visiteur</h2>
                                <p class="text-muted text-center"><span class="h1 text-dark">1€</span><span class="ml-2">/ mois</span></p>
                                <hr/>
                                <ul class="fa-ul my-4">
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>voir les annonces et répondre</li>
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>1 Photos</li>
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>500 caractères</li>
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Messagerie</li>
                                    <div class="text-center">
                                        <a href="{{ route('paypal.express-checkout', ['option' => 'N9TT-9G0A-B7FQ-RANC']) }}" class="btn btn-outline-primary">Choisir</a>
                                    </div>
                                </ul>

                            </div>
                        </div>
                        <!-- /pricing card -->
                    </div>

                    <div class="col-lg-4">
                        <!-- pricing card-->
                        <div class="card mb-5 mb-lg-0 border-0 shadow">
                            <div class="card-body">
                                <h2 class="text-base subtitle text-center text-primary py-3">Bonze</h2>
                                <p class="text-muted text-center"><span class="h1 text-dark">2€</span><span class="ml-2">/ mois</span></p>
                                <hr/>
                                <ul class="fa-ul my-4">
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>voir les annonces et répondre</li>
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>1 Photo</li>
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>800 caractères</li>
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Messaegie</li>

                                    <div class="text-center">
                                            <a href="{{ route('paypal.express-checkout', ['option' => 'QK6A-JI6S-7ETR-0A6C']) }}" class="btn btn-outline-primary">Choisir</a>
                                    </div>

                                </ul>

                            </div>
                        </div>
                        <!-- /pricing card -->
                    </div>
                    <div class="col-lg-4">
                        <!-- pricing card-->
                        <div class="card mb-5 mb-lg-0 border-0 shadow">
                            <div class="card-status bg-primary"></div>
                            <div class="card-body">
                                <h2 class="text-base subtitle text-center text-primary py-3">Argent</h2>
                                <p class="text-muted text-center"><span class="h1 text-dark">4€</span><span class="ml-2">/ mois</span></p>
                                <hr/>
                                <ul class="fa-ul my-4">
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Voir et repondre aux annonces</li>
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>3 photos</li>
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>1000 caractères</li>
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Messagerie</li>


                                </ul>
                                <div class="text-center">
                                    <a href="{{ route('paypal.express-checkout', ['option' => 'SXFP-CHYK-ONI6-S89U']) }}" class="btn btn-outline-primary">Choisir</a>
                                </div>

                            </div>
                        </div>
                        <!-- /pricing card -->
                    </div>
                    <div class="col-lg-4">
                        <!-- pricing card-->
                        <div class="card mb-5 mb-lg-0 border-0 shadow">
                            <div class="card-body">
                                <h2 class="text-base subtitle text-center text-primary py-3">Or</h2>
                                <p class="text-muted text-center"><span class="h1 text-dark">5€</span><span class="ml-2">/ moi</span></p>
                                <hr/>
                                <ul class="fa-ul my-4">
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>2 Annonces</li>

                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Voir et repondre aux annonces</li>
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>5 photos</li>
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>2000 caractères</li>
                                    <li class="mb-3"><span class="fa-li text-primary"><i class="fas fa-check"></i></span>Messagerie</li>
                                </ul>
                                <div class="text-center">
                                    <a href="{{ route('paypal.express-checkout', ['option' => 'XNSS-HSJW-3NGU-8XTJ']) }}" class="btn btn-outline-primary">Choisir</a>
                                </div>

                            </div>
                        </div>
                        <!-- /pricing card -->
                    </div>
                @endif
            </div>
        </div>
    </section>

@endsection

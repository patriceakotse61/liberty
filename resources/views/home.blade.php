@extends('layouts.app')
@section('content')




    <section class="hero-home">
        <div class="swiper-container hero-slider">
            <div class="swiper-wrapper dark-overlay">
                <div style="background-image:url(img/photo/rotate5.jpg)" class="swiper-slide"></div>
                <?php if ($mode_children == false ) { ?>
                <div style="background-image:url(img/photo/rotate1.jpg)" class="swiper-slide"></div>
                <?php } ?>
                <div style="background-image:url(img/photo/rotate6.jpg)" class="swiper-slide"></div>
                <?php if ($mode_children == false ) { ?>
                <div style="background-image:url(img/photo/rotate2.jpg)" class="swiper-slide"></div>
                <?php } ?>
                <div style="background-image:url(img/photo/rotate7.jpg)" class="swiper-slide"></div>
                <?php if ($mode_children == false ) { ?>
                <div style="background-image:url(img/photo/rotate3.jpg)" class="swiper-slide"></div>
                <div style="background-image:url(img/photo/rotate4.jpg)" class="swiper-slide"></div>
                <?php } ?>
            </div>
        </div>
        <?php if ($mode_children == false ) { ?>
        <div class="z-index-30 d-none d-sm-block cate-left-menu ">
            <?php
            foreach ($categories as $key => $data){
                //http://10.5.0.5/annonces?search=&region=auvergne&category=12

             echo '<a href="annonces?search=&region=&category='.$key.'" class="cate-left-link">';
             echo  __('liberty.'.$data);
             echo '</a><br/>';
             }

            echo '<br/>';
            ?>

        </div>
        <?php } ?>

        <div class="container py-6 py-md-7 text-white z-index-20">
            <div class="row">
                <div class="col-xl-10">
                    <div class="text-center text-lg-left">
                        <!--
                        <p class="subtitle letter-spacing-4 mb-2 text-secondary text-shadow">La meilleure expérience
                            d'annonce libertine de la caraîbe</p>
                            -->
                        <h1 class="display-3 font-weight-bold text-shadow">The World Connexion <br/>{{ __('liberty.connect-world') }}</h1>

                    </div>
                    <div class="search-bar mt-5 p-3 p-lg-1 pl-lg-4">

                        <form id="findAdForm" method="get" action={{ route('annonces.index') }}>


                            <div class="row">
                                <div class="col-lg-4 d-flex align-items-center form-group">
                                    <input type="text" name="search" placeholder="mot clé de votre recherche ?"
                                           class="form-control border-0 shadow-0">
                                </div>
                                <div class="col-lg-3 d-flex align-items-center form-group no-divider">
                                    <?php echo Form::select('region', $regions, null, ['data-style' => 'btn-form-control', 'class' => 'selectpicker']); ?>

                                        <i class="fa fa-crosshairs"></i>
                                </div>
                                <div class="col-lg-3 d-flex align-items-center form-group no-divider">
                                    <?php // echo Form::select('category', $categories, null, ['data-style' => 'btn-form-control', 'class' => 'selectpicker']); ?>
                                    <select data-style="btn-form-control" class="selectpicker" name="category" tabindex="-98">
                                    <?php
                                        foreach ($categories as $key => $data)
                                        {
                                            echo '<option value='.$key.'>'.__('liberty.'.$data).'</option>';
                                        }
                                    ?>
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <button type="submit" class="btn btn-primary btn-sm lock rounded-xl h-100">Trier</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- article et publication -->
    <section class="py-6 bg-gray-100">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8">
                    <p class="subtitle text-secondary">{{ __('liberty.info') }}</p>
                    <h2>{{ __('liberty.section') }}</h2>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <h4>{{ __('liberty.news') }}</h4>
                        <div class="align-top">
                            <video src="video/{{ $video }}"  controls width="100%" height="300px"></video>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h4>{{ __('liberty.coverage') }}</h4>
                        @foreach($coverage as $data)
                            <div class="" style="height: 300px">
                                @include('news.coverage', ['news' =>$data])
                            </div>
                        @endforeach
                    </div>
                    <div class="col-md-3">
                        <h4>{{ __('liberty.feed') }}</h4>
                        <div class="overflow-auto" style="height: 300px">
                            @foreach($news as $data)
                                @include('news.new_home', ['news' =>$data])
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </section>

    <section class="py-6 bg-gray-100">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8">
                    <p class="subtitle text-secondary">{{ __('liberty.tips') }}</p>
                    <h2>{{ __('liberty.announce') }}</h2>
                </div>
                <div class="col-md-4 d-lg-flex align-items-center justify-content-end">
                    <a href={{ '/annonces'  }} class="text-muted text-sm">
                    {{ __('liberty.listing') }}
                    <i class="fas fa-angle-double-right ml-2"></i>
                    </a>
                </div>
            </div>

            <div class="row">

                @foreach($ads as $ad)
                    <div class="col-md-3 col-sm-12 pt-3 mt-2">
                        @include('partials.ad', ['ad' =>$ad])
                    </div>
                @endforeach

            </div>

            <!--
            <div
                data-swiper="{&quot;slidesPerView&quot;:4,&quot;spaceBetween&quot;:20,&quot;loop&quot;:true,&quot;roundLengths&quot;:true,&quot;breakpoints&quot;:{&quot;1200&quot;:{&quot;slidesPerView&quot;:3},&quot;991&quot;:{&quot;slidesPerView&quot;:2},&quot;565&quot;:{&quot;slidesPerView&quot;:1}},&quot;pagination&quot;:{&quot;el&quot;:&quot;.swiper-pagination&quot;,&quot;clickable&quot;:true,&quot;dynamicBullets&quot;:true}}"
                class="swiper-container swiper-container-mx-negative swiper-init pt-3">
                <div class="swiper-wrapper pb-5">

                    foreach($ads as $ad)
                            <div class="swiper-slide h-auto px-2 hover-animate">
                            include('partials.ad', ['ad' =>$ad])
                            </div>
                    endforeach

                </div>
                <div class="swiper-pagination"></div>
            </div>
            -->
        </div>
    </section>

    <!-- Divider Section-->
    <section class="py-7 position-relative dark-overlay">
        <?php if ($mode_children == false ) { ?>
        <img src="img/photo/photo-couple.jpg" alt="" class="bg-image">
        <?php } ?>
        <div class="container">
            <div class="overlay-content text-white py-lg-5">
                <h3 class="display-3 font-weight-bold text-shadow mb-5">{{ __('liberty.ads') }}</h3>
            <!-- <a href="{{ route('dopage') }}" class="btn btn-light">Je réserve ma place</a>-->
            </div>
        </div>
    </section>

    <section class="py-6">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8">
                    <p class="subtitle text-primary">QUELQUES RÉGIONS </p>
                    <h2></h2>
                </div>
                <div class="col-md-4 d-md-flex align-items-center justify-content-end"><a href="#" class="text-muted text-sm">

                        <i class="fas fa-angle-double-right ml-2"></i></a></div>
            </div>
            <div class="row">
                <div class="d-flex align-items-lg-stretch mb-4 col-lg-8">
                    <div style="background: center center url(img/region/MONDE.jpg) no-repeat; background-size: cover;" class="card shadow-lg border-0 w-100 border-0 hover-animate">
                        <a href="{{ url('annonces/') }}" class="tile-link"> </a>
                        <div class="d-flex align-items-center h-100 text-white justify-content-center py-6 py-lg-7">
                            <h3 class="text-shadow text-uppercase mb-0">VOIR TOUTES LES ANNONCES</h3>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-lg-stretch mb-4 col-lg-4">
                    <div style="background: center center url(img/region/MARTINIQUE.jpg) no-repeat; background-size: cover;" class="card shadow-lg border-0 w-100 border-0 hover-animate">
                        <a href="{{ url('annonces?search=&region=martinique') }}" class="tile-link"> </a>
                        <div class="d-flex align-items-center h-100 text-white justify-content-center py-6 py-lg-7">
                            <h3 class="text-shadow text-uppercase mb-0">MARTINIQUE</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="d-flex align-items-lg-stretch mb-4 col-lg-4">
                    <div style="background: center center url(img/region/PARIS.jpg) no-repeat; background-size: cover;" class="card shadow-lg border-0 w-100 border-0 hover-animate">
                        <a href="{{ url('annonces?search=&region=ile_de_france&departement=75') }}" class="tile-link"> </a>
                        <div class="d-flex align-items-center h-100 text-white justify-content-center py-6 py-lg-7">
                            <h3 class="text-shadow text-uppercase mb-0">PARIS</h3>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-lg-stretch mb-4 col-lg-4">
                    <div style="background: center center url(img/region/LYON.jpg) no-repeat; background-size: cover;" class="card shadow-lg border-0 w-100 border-0 hover-animate">
                        <a href="{{ url('annonces?search=&region=provence&departement=13&commune=13055') }}" class="tile-link"> </a>
                        <div class="d-flex align-items-center h-100 text-white justify-content-center py-6 py-lg-7">
                            <h3 class="text-shadow text-uppercase mb-0">MARSEILLE</h3>
                        </div>
                    </div>
                </div>
                <div class="d-flex align-items-lg-stretch mb-4 col-lg-4">
                    <div style="background: center center url(img/region/GUADELOUPE.jpg) no-repeat; background-size: cover;" class="card shadow-lg border-0 w-100 border-0 hover-animate">
                        <a href="{{ url('annonces?search=&region=guadeloupe') }}" class="tile-link"> </a>
                        <div class="d-flex align-items-center h-100 text-white justify-content-center py-6 py-lg-7">
                            <h3 class="text-shadow text-uppercase mb-0">GUADELOUPE</h3>
                        </div>
                    </div>
                </div>
            </div>

        </div><br>
    </section>


    <!--
    <section class="py-6 bg-gray-100">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8">
                    <p class="subtitle text-secondary">ENVIE DE FAIRE LA FÈTE ?</p>
                    <h2>Les bons plan de soirée hot</h2>
                </div>
                <div class="col-md-4 d-lg-flex align-items-center justify-content-end">
                    <a href={{ '/annonces'  }} class="text-muted text-sm">
                    Voir toutes les annonces<i class="fas fa-angle-double-right ml-2"></i>
                    </a>
                </div>
            </div>

            <div
                data-swiper="{&quot;slidesPerView&quot;:3,&quot;spaceBetween&quot;:20,&quot;loop&quot;:true,&quot;roundLengths&quot;:true,&quot;breakpoints&quot;:{&quot;1200&quot;:{&quot;slidesPerView&quot;:3},&quot;991&quot;:{&quot;slidesPerView&quot;:2},&quot;565&quot;:{&quot;slidesPerView&quot;:1}},&quot;pagination&quot;:{&quot;el&quot;:&quot;.swiper-pagination&quot;,&quot;clickable&quot;:true,&quot;dynamicBullets&quot;:true}}"
                class="swiper-container swiper-container-mx-negative swiper-init pt-3">
                <div class="swiper-wrapper pb-5">
                    foreach($adsClub as $ad)
                        <div class="swiper-slide cd h-auto px-2 hover-animate">
                            include('partials.ad', ['ad' =>$ad])
                        </div>
                    endforeach

                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>
    -->


    <section class="py-6 bg-gray-100">
        <div class="container">
<!--
            <center><h3>DECROUVEZ VOTRE ACTUALITÉ</h3></center>
-->
        </div>
    </section>
    <!-- Instagram-->
    <section>
        <div class="container-fluid px-0">
            <div class="swiper-container instagram-slider">
                <div class="swiper-wrapper">

                    <?php for ($i = 1; $i < 20; $i++) { ?>
                    <div class="swiper-slide overflow-hidden"><a href="#"><img src="img/instagram/<?php echo $i ?>.jpg" alt="" class="img-fluid hover-scale"></a></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection

@extends('layouts.app')
@section('content')
    <div class="container py-5">
        <div id="massageOk" class="alert alert-success" role="alert" style="display: none">
            Votre message a été pris en compte et sera envoyé rapidement
        </div>
        @include('partials.message', ['url' => route('message')])
        @include('chat.modal')

        <div class="row">
            <div class="col-lg-8">
                <div class="text-block">
                    <p class="text-primary">
                        <i class="fa-map-marker-alt fa mr-1"></i>
                        {{ $ad->commune_name . ' (' . $ad->commune_postal . ')'}}
                    </p>
                    <h1>{{ $ad->title }}</h1>
                    <p class="text-muted text-uppercase mb-4">{{ $ad->category->name }}</p>
                    <ul class="list-inline text-sm mb-4">
                        <li class="list-inline-item mr-3">
                            <i class="fa fa-user mr-1 text-secondary"></i>
                            Annonce déposée par {{ $ad->pseudo }} le {{ $ad->created_at->calendar() }}
                        </li>
                    </ul>
                    <p class="text-muted font-weight-light">
                        {!! $ad->texte !!}
                    </p>
                </div>

                <div class="text-block">
                    <h5 class="mb-4" >Ma galerie photo</h5>
                    <div class="row gallery mb-3 ml-n1 mr-n1">
                        @if($photos->isNotEmpty())
                            @if($photos->count() > 0)
                                    @foreach ($photos as $photo)
                                        <div class="col-lg-4 col-6 px-1 mb-2">
                                            <a href="{{ asset('/images/' . $photo->filename) }}" data-fancybox="gallery" title="Outside">
                                                <img src="{{ asset('/images/' . $photo->filename) }}" alt="..." class="img-fluid">
                                            </a>
                                        </div>
                                    @endforeach
                            @endif
                        @endif
                    </div>
                </div>

                <!--
                <div class="text-block">
                    <p class="subtitle text-sm text-primary">Avis reçut de cette annonce</p>
                    <h5 class="mb-4">Liste des commentaires</h5>
                    <div class="media d-block d-sm-flex review">
                        <div class="text-md-center mr-4 mr-xl-5"><img src="img/avatar/avatar-8.jpg" alt="Padmé Amidala" class="d-block avatar avatar-xl p-2 mb-2"><span class="text-uppercase text-muted text-sm">Dec 2018</span></div>
                        <div class="media-body">
                            <h6 class="mt-2 mb-1">Marine cottreau</h6>
                            <div class="mb-2"><i class="fa fa-xs fa-star text-primary"></i><i class="fa fa-xs fa-star text-primary"></i><i class="fa fa-xs fa-star text-primary"></i><i class="fa fa-xs fa-star text-primary"></i><i class="fa fa-xs fa-star text-primary"></i>
                            </div>
                            <p class="text-muted text-sm">woow bzh hozhoazf j'ai adoré shf hsofhs oh shf jhqosfoqhsoifhoqhsiqoishfoiqhosihfqoihsoihqoifhsoiqhsifhqosihfoqihsoqhsiohf    </p>
                        </div>
                    </div>
                    <div class="media d-block d-sm-flex review">
                        <div class="text-md-center mr-4 mr-xl-5"><img src="img/avatar/avatar-2.jpg" alt="Luke Skywalker" class="d-block avatar avatar-xl p-2 mb-2"><span class="text-uppercase text-muted text-sm">Dec 2018</span></div>
                        <div class="media-body">
                            <h6 class="mt-2 mb-1">Patrick le borgne</h6>
                            <div class="mb-2"><i class="fa fa-xs fa-star text-primary"></i><i class="fa fa-xs fa-star text-primary"></i><i class="fa fa-xs fa-star text-primary"></i><i class="fa fa-xs fa-star text-primary"></i><i class="fa fa-xs fa-star text-gray-200"></i>
                            </div>
                            <p class="text-muted text-sm">woooooooooq   ofh oazhf oahzofha oizhfoiah ezfuzoa puhez pfgzfu  pazoue fpugapuzge fpuagz epufguagz epufgzug epoif gapugz peufaz uepf  iauzg epfiugzeuf piauzg epufgapiuzgepfugzpiuegfiuzeg fpazieugf pizug efpiuaz pf aze    </p>
                        </div>
                    </div>

                    <div class="py-5">
                        <div id="leaveReview" class="collapse mt-4">
                            <h5 class="mb-4">Posté un commentaire</h5>
                            <form id="contact-form" method="get" action="#" class="form">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="name" class="form-label">Votre nom</label>
                                            <input type="text" name="name" id="name" placeholder="Votre nom" required="required" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="rating" class="form-label">Votre notre</label>
                                            <select name="rating" id="rating" class="custom-select focus-shadow-0">
                                                <option value="5">&#9733;&#9733;&#9733;&#9733;&#9733; (5/5)</option>
                                                <option value="4">&#9733;&#9733;&#9733;&#9733;&#9734; (4/5)</option>
                                                <option value="3">&#9733;&#9733;&#9733;&#9734;&#9734; (3/5)</option>
                                                <option value="2">&#9733;&#9733;&#9734;&#9734;&#9734; (2/5)</option>
                                                <option value="1">&#9733;&#9734;&#9734;&#9734;&#9734; (1/5)</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="form-label">Votre mail</label>
                                    <input type="email" name="email" id="email" placeholder="Votre mail" required="required" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="review" class="form-label">Commentaire</label>
                                    <textarea rows="4" name="review" id="review" placeholder="Votre commentaire" required="required" class="form-control"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Poster</button>
                            </form>
                        </div>
                    </div>
                </div>
                -->
            </div>
            <div class="col-lg-4">
                <div style="top: 100px;" class="p-4 shadow ml-lg-4 rounded sticky-top">
                    <!--
                    <p class="text-muted"><span class="text-primary h2">{{ $ad->prix }} €</span> par heure</p>
                    <hr class="my-4">
                    -->
                    <form id="booking-form" method="get" action="#" autocomplete="off" class="form">
                        @if (isset(auth()->user()->subscription))
                        <div class="form-group">
                            <center><labe>Appeler au </labe></center>
                            <button type="submit" class="btn btn-success btn-block">{{ $ad->telephone }}</button>
                        </div>
                        @endif
                        <div class="form-group">
                            <button id="openModal" type="button" class="btn btn-primary">Repondre à l'annonce</button>
                        </div>
                    </form>
                    @if (isset(auth()->user()->subscription))
                    <div class="form-group">
                        <button id="openChatModal" type="button" class="btn btn-secondary">discuter avec cette personne</button>
                    </div>
                    @endif
                    <!--
                    <hr class="my-4">
                    <div class="text-center">
                        <p> <a href="#" class="text-secondary text-sm"> <i class="fa fa-heart"></i>Déposer un coud de coeur</a></p>
                        <p class="text-muted text-sm">79 personnes ont kiffé cette annonce </p>
                    </div>
                    -->
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script src="{{ asset('js/vue.js') }}"></script>
    <script>
        $(() => {
            const toggleButtons = () => {
                $('#icon').toggle();
                $('#buttons').toggle();
            }
            $('#openModal').click(() => {
                $('#messageModal').modal();
            });
            $('#openChatModal').click(() => {
                $('#chatModal').modal();
            });

            $('#messageForm').submit((e) => {
                let that = e.currentTarget;
                e.preventDefault();
                $('#message').removeClass('is-invalid');
                $('.invalid-feedback').html('');
                toggleButtons();
                $.ajax({
                    method: $(that).attr('method'),
                    url: $(that).attr('action'),
                    data: $(that).serialize()
                })
                    .done((data) => {
                        toggleButtons();
                        $('#messageModal').modal('hide');
                        $('#massageOk').text(data.info).show();
                    })
                    .fail((data) => {
                        toggleButtons();
                        $.each(data.responseJSON.errors, function (i, error) {
                            $(document)
                                .find('[name="' + i + '"]')
                                .addClass('is-invalid')
                                .next()
                                .append(error[0]);
                        });
                    });
            });
        })
    </script>
@endsection



@extends('layouts.app')
@section('content')
    <div class="container py-5">

        <h4>Conditions d'utilisation</h4></br>
        Le site accessible par les url suivants : </br><strong>www.libert-iles.com</strong> est exploité dans le respect de la législation française. L'utilisation de ce site est régie par les
        présentes conditions générales. En utilisant le site, vous reconnaissez avoir pris connaissance de ces conditions et les avoir acceptées. Celles-ci pourront êtres modifiées à tout moment et
        sans préavis par la structure libert’île.
        Libert’île ne saurait être tenu pour responsable en aucune manière d’une mauvaise utilisation du service. <br/>

        Limitation de responsabilité
        Les informations contenues sur ce site sont aussi précises que possibles et le site est périodiquement remis à jour, mais peut toutefois contenir des inexactitudes, des omissions ou des
        lacunes. Si vous constatez une lacune, erreur ou ce qui parait être un dysfonctionnement, merci de bien vouloir le signaler par email en décrivant le problème de la manière la plus précise
        possible (page posant problème, action déclenchante, type d’ordinateur et de navigateur utilisé, …).

        Tout contenu téléchargé se fait aux risques et périls de l'utilisateur et sous sa seule responsabilité. En conséquence, Libert’île ne saurait être tenu responsable d'un quelconque dommage subi
        par l'ordinateur de l'utilisateur ou d'une quelconque perte de données consécutives au téléchargement.

        Les photos sont non contractuelles.

        Les liens hypertextes mis en place dans le cadre du présent site internet en direction d'autres ressources présentes sur le réseau Internet ne sauraient engager la responsabilité de
        Libert’île.

        Litiges
        Les présentes conditions sont régies par les lois françaises et toute contestation ou litiges qui pourraient naître de l'interprétation ou de l'exécution de celles-ci seront de la compétence
        exclusive des tribunaux dont dépend le siège social de Libert’îles net. La langue de référence, pour le règlement de contentieux éventuels, est le français.
        Déclaration à la CNIL
        Conformément à la <strong>loi 78-17 du 6 janvier 1978 </strong>(modifiée par la loi 2004-801 du 6 août 2004 relative à la protection des personnes physiques à l'égard des traitements de
        données à caractère personnel) relative à l'informatique, aux fichiers et aux libertés, le site a fait l'objet d'une déclaration auprès de la Commission nationale de l'informatique et des
        libertés <strong>(www.cnil.fr)</strong>.
        Droit d'accès
        En application de cette loi, les internautes disposent d’un droit d’accès, de rectification, de modification et de suppression concernant les données qui les concernent personnellement. Ce
        droit peut être exercé par mail auprès de libert’île.

        Les informations personnelles collectées ne sont en aucun cas confiées à des tiers hormis pour l’éventuelle bonne exécution de la prestation commandée par l’internaute.
        </br></br>
        <h4>Confidentialité</h4>

        Vos données personnelles sont confidentielles et ne seront en aucun cas communiquées à des tiers hormis pour la bonne exécution de la prestation.
        Propriété intellectuelle.
        Tout le contenu du présent site, incluant, de façon non limitative, les graphismes, images, textes, vidéos, animations, sons, logos, gifs et icônes ainsi que leur mise en forme sont la
        propriété exclusive de la structure <strong>libert’île</strong> net à l'exception des marques, logos ou contenus appartenant à d'autres sociétés partenaires ou auteurs.
        Toute reproduction, distribution, modification, adaptation, retransmission ou publication, même partielle, de ces différents éléments est strictement interdite sans l'accord exprès par écrit
        de Natural net. Cette représentation ou reproduction, par quelque procédé que ce soit, constitue une contrefaçon sanctionnée par <strong> les articles L.3335-2</strong> et suivants du Code de
        la propriété intellectuelle. Le non-respect de cette interdiction constitue une contrefaçon pouvant engager la responsabilité civile et pénale du contrefacteur. En outre, les propriétaires des
        Contenus copiés pourraient intenter une action en justice à votre encontre.

        https://www.libert-iles.com est identiquement propriétaire des "droits des producteurs de bases de données" visés au Livre III, Titre IV, du Code de la Propriété Intellectuelle <strong> (loi
            n° 98-536 du 1er juillet 1998)</strong> relative aux droits d'auteur et aux bases de données.

        Les utilisateurs et visiteurs du site internet peuvent mettre en place un hyperlien en direction de ce site, mais uniquement en direction de la page d’accueil, accessible à l’URL suivante
        :</br><strong>>www.libert-iles.com </strong> à condition que ce lien s’ouvre dans une nouvelle fenêtre.

        Par ailleurs, la mise en forme de ce site a nécessité le recours à des sources externes dont nous avons acquis les droits ou dont les droits d’utilisation.
        Hébergeur OVH

        Plateforme de gestion et création de sites internet
        </br></br>
        <h4>Conditions de service</h4>
        Ce site est proposé en langages et techno HTML5 et CSS3, et LARAVEL pour un meilleur confort d'utilisation et un graphisme plus agréable, nous vous recommandons de recourir à des navigateurs
        modernes comme Safari, Firefox, Chrome,...
        Informations et exclusion
        Libert’iles met en œuvre tous les moyens dont ils dispose, pour assurer une information fiable et une mise à jour fiable de ses sites internet. Toutefois, des erreurs ou omissions peuvent
        survenir. L'internaute devra donc s'assurer de l'exactitude des informations auprès de libert’île, et signaler toutes modifications du site qu'il jugerait utile.

        <strong>Libert’îles</strong> n'est en aucun cas responsable de l'utilisation faite de ces informations, et de tout préjudice direct ou indirect pouvant en découler.
        Cookies
        Pour des besoins de statistiques et d'affichage, le présent site utilise des cookies. Il s'agit de petits fichiers textes stockés sur votre disque dur afin d'enregistrer des données techniques
        sur votre navigation. Certaines parties de ce site ne peuvent être fonctionnelle sans l’acceptation de cookies.</br>
        <h4>Liens hypertextes</h4>
        Les sites internet de Libert’île peuvent offrir des liens vers d’autres sites internet ou d’autres ressources disponibles sur Internet.
    </div>

    </div>
@endsection

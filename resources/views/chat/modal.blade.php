<div class="modal fade" id="chatModal" tabindex="-1" role="dialog" aria-labelledby="message" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">


            <!-- Chat Box -->
            <div class="col-12 px-0">

                <div id="app" class="">
                    <div class="row ">

                        <!-- Chat Box -->
                        <div class="col-12 px-0">

                            @if (isset(auth()->user()->id))
                                <input type="hidden" id="userfrom" value="{{ auth()->user()->id }}">
                                <input type="hidden" id="userto" value="{{ $ad->user_id }}">

                                <chat-messages
                                    :messages="messages"
                                    v-bind:userfrom="{{ auth()->user()->id }}"
                                    v-bind:userto="{{ $ad->user_id }}"
                                ></chat-messages>

                                <chat-form
                                    v-on:messagesent="addMessage"
                                    v-bind:userfrom2="{{ auth()->user()->id }}"
                                    v-bind:userto2="{{ $ad->user_id }}"
                                ></chat-form>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="card h-100 border-0 shadow card-zoom">
    <div class="card-img-top overflow-hidden grient-overlay ad-card-img-div-top">
        @if($news->photos->isNotEmpty())
            <img src="{{ asset('thumbs/' . $news->photos->first()->filename) }}" alt="" class="rounded img-fluid" width="100%">
        @else
            <img src="{{ asset('thumbs/nopicture.jpg') }}" alt="" class="img-fluid" width="100%">
        @endif
        <a href="{{ route('annonces.show', $news->id) }}" class="tile-link"></a>
        <!--
    <div class="card-img-overlay-top text-right"><a href="javascript: void();" class="card-fav-icon position-relative z-index-40">
            <svg class="svg-icon text-white">
                <use xlink:href="#heart-1"> </use>
            </svg></a>
    </div>
    -->
    </div>
    <div class="card-body d-flex align-items-center ">
        <div class="w-100">
            <h6 class="card-title">{{ $news->title }}</h6>
            <p class="text-gray-700 text-sm my-3 ad-text">

                {{ strlen($news->texte) > 200 ? substr($news->texte, 0, 200).'...' : $news->texte }}
            </p>

            <a href="{{ route('news.show', $news->id) }}" class="tile-link"></a>
        </div>
    </div>
</div>

<div class="card h-60 border-0 shadow card-zoom  overflow-hidden" >
    <div class="row">

        <div class="col-md-5">
            @if($news->photos->isNotEmpty())
                <img src="{{ asset('thumbs/' . $news->photos->first()->filename) }}" alt="" class="rounded img-fluid" width="100%">
            @else
                <img src="{{ asset('thumbs/nopicture.jpg') }}" alt="" class="img-fluid">
            @endif
            <a href="{{ route('news.show', $news->id) }}" class="tile-link"></a>

        </div>
        <div class="col-md-7" style="padding: 0; margin: 0">
            <div class="text-gray-700 text-sm my-3 ad-text">

                {{ strlen($news->title) > 35 ? substr($news->title, 0, 35).'...' : $news->title }}
            </div>
            <a href="{{ route('news.show', $news->id) }}" class="tile-link"></a>
        </div>
    </div>

</div>

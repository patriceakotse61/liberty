<div class="card h-100 border-0 shadow card-zoom">
    <div class="card-img-top overflow-hidden grient-overlay ad-card-img-div-top">
        @if($news->photos->isNotEmpty())
            <img src="{{ asset('thumbs/' . $news->photos->first()->filename) }}" alt="" class="rounded img-fluid" width="100%">
        @else
            <img src="{{ asset('thumbs/nopicture.jpg') }}" alt="" class="img-fluid" width="100%">
        @endif
        <a href="{{ route('annonces.show', $news->id) }}" class="tile-link"></a>
    </div>
    <div class="card-by d-flex align-bottom carg-overlay">
        <div class="w-100 coverage-back">
            <div class="card-title">{{ $news->title }}</div>

            <a href="{{ route('news.show', $news->id) }}" class="tile-link"></a>
        </div>
    </div>
</div>

<div class="card h-100 border-0 shadow card-zoom">
    <div class="card-img-top overflow-hidden grient-overlay ad-card-img-div-top">
        @if($ad->photos->isNotEmpty())
            <img src="{{ asset('thumbs/' . $ad->photos->first()->filename) }}" alt="" class="rounded img-fluid" width="100%">
        @else
            <img src="{{ asset('thumbs/nopicture.jpg') }}" alt="" class="img-fluid">
        @endif
        <a href="{{ route('annonces.show', $ad->id) }}" class="tile-link"></a>
        <!--
    <div class="card-img-overlay-top text-right"><a href="javascript: void();" class="card-fav-icon position-relative z-index-40">
            <svg class="svg-icon text-white">
                <use xlink:href="#heart-1"> </use>
            </svg></a>
    </div>
    -->
    </div>
    <div class="card-body d-flex align-items-center">
        <div class="w-100">
            <h6 class="card-title">{{ $ad->title }}</h6>
            <div class="d-flex card-subtitle mb-3">
                <p class="flex-grow-1 mb-0 text-muted text-sm ad-text-small">{{ $ad->category->name }}</p>
                <!--
                <p class="flex-shrink-1 mb-0 card-stars text-xs text-right">
                    <i class="fa fa-star text-warning"></i>
                    <i class="fa fa-star text-warning"></i>
                    <i class="fa fa-star text-warning"></i>
                    <i class="fa fa-star text-gray-300"></i>
                    <i class="fa fa-star text-gray-300"></i>
                </p>
                -->
            </div>
            <p class="text-gray-700 text-sm my-3 ad-text">

                {{ strlen($ad->texte) > 50 ? substr($ad->texte, 0, 50).'...' : $ad->texte }}
            </p>

            <p class="text-gray-500 text-sm my-3 align-bottom ad-text-small">
                <i class="fa fa-road mr-2"></i>{{ $ad->commune_name . ' (' . $ad->commune_postal . ')'}}<br>
                <i class="far fa-clock mr-2"></i>{{ $ad->created_at->format('d/m/Y H:i') }}
            </p>
            <a href="{{ route('annonces.show', $ad->id) }}" class="tile-link"></a>
        </div>
    </div>
</div>


<div class="row">
@foreach($ads as $ad)
        <div class="col-sm-6 col-lg-4 mb-5 hover-animate">
        @include('partials.ad', ['ad' =>$ad])
        </div>
@endforeach
</div>

<div class="d-flex">
    <div class="mx-auto">
        {{ $ads->links() }}
    </div>
</div>

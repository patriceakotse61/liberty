@extends('layouts.app')
@section('content')




    <section class="hero-home">
        <div class="swiper-container hero-slider">
            <div class="swiper-wrapper dark-overlay">
                <div style="background-image:url(img/photo/rotate5.jpg)" class="swiper-slide"></div>
                <div style="background-image:url(img/photo/rotate1.jpg)" class="swiper-slide"></div>
                <div style="background-image:url(img/photo/rotate6.jpg)" class="swiper-slide"></div>
                <div style="background-image:url(img/photo/rotate2.jpg)" class="swiper-slide"></div>
                <div style="background-image:url(img/photo/rotate7.jpg)" class="swiper-slide"></div>
                <div style="background-image:url(img/photo/rotate3.jpg)" class="swiper-slide"></div>
                <div style="background-image:url(img/photo/rotate4.jpg)" class="swiper-slide"></div>
            </div>
        </div>
        <div class="z-index-30 d-none d-sm-block cate-left-menu ">
            <?php
            foreach ($categories as $key => $data){
                //http://10.5.0.5/annonces?search=&region=auvergne&category=12

             echo '<a href="annonces?search=&region=&category='.$key.'" class="cate-left-link">'.$data.'</a><br/>';
             }

            echo '<br/>';
            ?>

        </div>

        <div class="container py-6 py-md-7 text-white z-index-20">
            <div class="row">
                <div class="col-xl-10">
                    <div class="text-center text-lg-left">
                        <!--
                        <p class="subtitle letter-spacing-4 mb-2 text-secondary text-shadow">La meilleure expérience
                            d'annonce libertine de la caraîbe</p>
                            -->
                        <h1 class="display-3 font-weight-bold text-shadow">Votre monde connectée</h1>
                    </div>
                    <div class="search-bar mt-5 p-3 p-lg-1 pl-lg-4">

                        <form id="findAdForm" method="get" action={{ route('annonces.index') }}>


                            <div class="row">
                                <div class="col-lg-4 d-flex align-items-center form-group">
                                    <input type="text" name="search" placeholder="mot clé de votre recherche ?"
                                           class="form-control border-0 shadow-0">
                                </div>
                                <div class="col-lg-3 d-flex align-items-center form-group no-divider">
                                    <?php echo Form::select('region', $regions, null, ['data-style' => 'btn-form-control', 'class' => 'selectpicker']); ?>

                                        <i class="fa fa-crosshairs"></i>
                                </div>
                                <div class="col-lg-3 d-flex align-items-center form-group no-divider">
                                    <?php echo Form::select('category', $categories, null, ['data-style' => 'btn-form-control', 'class' => 'selectpicker']); ?>
                                </div>
                                <div class="col-lg-2">
                                    <button type="submit" class="btn btn-primary btn-sm lock rounded-xl h-100">Trier</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--
    <section class="py-6">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8">
                    <p class="subtitle text-primary">Choisisez un lieux </p>
                    <h2>Vos meilleurs plans dans les DOM-TOM</h2>
                </div>
                <div class="col-md-4 d-lg-flex align-items-center justify-content-end">
                    <a href={{ url('annonces') }}  class="text-muted text-sm">
                        Voir toutes les régions
                        <i class="fas fa-angle-double-right ml-2"></i>
                    </a>
                </div>
            </div>
            <div class="swiper-container guides-slider mx-n2 pt-3">
                <div class="swiper-wrapper pb-5">
                    <div class="swiper-slide h-auto px-2">
                        <div class="card card-poster gradient-overlay hover-animate mb-4 mb-lg-0"><a
                                href={{ url('annonces/guadeloupe') }} class="tile-link"></a><img
                                src="img/photo/new-york.jpg" alt="Card image" class="bg-image">
                            <div class="card-body overlay-content">
                                <h6 class="card-title text-shadow text-uppercase">Guadeloupe</h6>
                                <p class="card-text text-sm">Le vice caché</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide h-auto px-2">
                        <div class="card card-poster gradient-overlay hover-animate mb-4 mb-lg-0"><a
                                href="{{ url('annonces/martinique') }}" class="tile-link"></a><img
                                src="img/photo/paris.jpg" alt="Card image" class="bg-image">
                            <div class="card-body overlay-content">
                                <h6 class="card-title text-shadow text-uppercase">Martinique</h6>
                                <p class="card-text text-sm">Les sans combine</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide h-auto px-2">
                        <div class="card card-poster gradient-overlay hover-animate mb-4 mb-lg-0"><a
                                href="{{ url('annonces/guyane') }}" class="tile-link"></a><img
                                src="img/photo/barcelona.jpg" alt="Card image" class="bg-image">
                            <div class="card-body overlay-content">
                                <h6 class="card-title text-shadow text-uppercase">Guyane</h6>
                                <p class="card-text text-sm">D</p>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide h-auto px-2">
                        <div class="card card-poster gradient-overlay hover-animate mb-4 mb-lg-0"><a
                                href="{{ url('annonces/guyane') }}" class="tile-link"></a><img
                                src="img/photo/barcelona.jpg" alt="Card image" class="bg-image">
                            <div class="card-body overlay-content">
                                <h6 class="card-title text-shadow text-uppercase">Guyane</h6>
                                <p class="card-text text-sm">D</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="swiper-pagination d-md-none"></div>
            </div>
        </div>
    </section>
    -->
    <section class="py-6 bg-gray-100">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8">
                    <p class="subtitle text-secondary">Les derniers bons plans </p>
                    <h2>Annonces récentes</h2>
                </div>
                <div class="col-md-4 d-lg-flex align-items-center justify-content-end">
                    <a href={{ '/annonces'  }} class="text-muted text-sm">
                    Voir toutes les annonces
                    <i class="fas fa-angle-double-right ml-2"></i>
                    </a>
                </div>
            </div>

            <div class="row">

                @foreach($ads as $ad)
                    <div class="col-md-3 col-sm-12 pt-3 mt-2">
                        @include('partials.ad', ['ad' =>$ad])
                    </div>
                @endforeach

            </div>

            <!--
            <div
                data-swiper="{&quot;slidesPerView&quot;:4,&quot;spaceBetween&quot;:20,&quot;loop&quot;:true,&quot;roundLengths&quot;:true,&quot;breakpoints&quot;:{&quot;1200&quot;:{&quot;slidesPerView&quot;:3},&quot;991&quot;:{&quot;slidesPerView&quot;:2},&quot;565&quot;:{&quot;slidesPerView&quot;:1}},&quot;pagination&quot;:{&quot;el&quot;:&quot;.swiper-pagination&quot;,&quot;clickable&quot;:true,&quot;dynamicBullets&quot;:true}}"
                class="swiper-container swiper-container-mx-negative swiper-init pt-3">
                <div class="swiper-wrapper pb-5">

                    foreach($ads as $ad)
                            <div class="swiper-slide h-auto px-2 hover-animate">
                            include('partials.ad', ['ad' =>$ad])
                            </div>
                    endforeach

                </div>
                <div class="swiper-pagination"></div>
            </div>
            -->
        </div>
    </section>
    <!-- Divider Section-->
    <section class="py-7 position-relative dark-overlay"><img src="img/photo/photo-1497436072909-60f360e1d4b1.jpg"
                                                              alt="" class="bg-image">
        <div class="container">
            <div class="overlay-content text-white py-lg-5">
                <h3 class="display-3 font-weight-bold text-shadow mb-5">Votre publicité ici </h3>
            <!-- <a href="{{ route('dopage') }}" class="btn btn-light">Je réserve ma place</a>-->
            </div>
        </div>
    </section>

    <!--
    <section class="py-6 bg-gray-100">
        <div class="container">
            <div class="row mb-5">
                <div class="col-md-8">
                    <p class="subtitle text-secondary">ENVIE DE FAIRE LA FÈTE ?</p>
                    <h2>Les bons plan de soirée hot</h2>
                </div>
                <div class="col-md-4 d-lg-flex align-items-center justify-content-end">
                    <a href={{ '/annonces'  }} class="text-muted text-sm">
                    Voir toutes les annonces<i class="fas fa-angle-double-right ml-2"></i>
                    </a>
                </div>
            </div>

            <div
                data-swiper="{&quot;slidesPerView&quot;:3,&quot;spaceBetween&quot;:20,&quot;loop&quot;:true,&quot;roundLengths&quot;:true,&quot;breakpoints&quot;:{&quot;1200&quot;:{&quot;slidesPerView&quot;:3},&quot;991&quot;:{&quot;slidesPerView&quot;:2},&quot;565&quot;:{&quot;slidesPerView&quot;:1}},&quot;pagination&quot;:{&quot;el&quot;:&quot;.swiper-pagination&quot;,&quot;clickable&quot;:true,&quot;dynamicBullets&quot;:true}}"
                class="swiper-container swiper-container-mx-negative swiper-init pt-3">
                <div class="swiper-wrapper pb-5">
                    foreach($adsClub as $ad)
                        <div class="swiper-slide cd h-auto px-2 hover-animate">
                            include('partials.ad', ['ad' =>$ad])
                        </div>
                    endforeach

                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </section>
    -->


    <section class="py-6 bg-gray-100">
        <div class="container">
<!--
            <center><h3>DECROUVEZ VOTRE ACTUALITÉ</h3></center>
-->
        </div>
    </section>
    <!-- Instagram-->
    <section>
        <div class="container-fluid px-0">
            <div class="swiper-container instagram-slider">
                <div class="swiper-wrapper">

                    <?php for ($i = 1; $i < 20; $i++) { ?>
                    <div class="swiper-slide overflow-hidden"><a href="#"><img src="img/instagram/<?php echo $i ?>.jpg" alt="" class="img-fluid hover-scale"></a></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('script')
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection

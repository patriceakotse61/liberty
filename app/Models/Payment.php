<?php

namespace App;

use Illuminate\Database\Eloquent\Model;



class Payment extends Model
{
    /**
     * Fields that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['transaction_id'];
}

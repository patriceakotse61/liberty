<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Route;

use App\Policies\AdPolicy;
use App\Models\Ad;

class AppServiceProvider extends ServiceProvider
{

    protected $policies = [
        Ad::class => AdPolicy::class
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::if ('admin', function () {
            return auth()->check() && auth()->user()->admin;
        });
        Route::resourceVerbs([
            'create' => 'creer',
            'edit' => 'modifier',
        ]);

    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;

class PaymentController extends Controller
{
    public function index()
    {

        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));


        $subscriptions = \Stripe\Plan::all([
            'product' => 'prod_GNV5Bw0JFzV5OA'
        ]);

        return view('payment', compact('subscriptions'));
    }

    public function charge(Request $request)
    {




        if ($request->input('stripeToken')) {

            \Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

            if (auth()->user()->stripe_customer === null)
            {
                // creation du client
                $customer = \Stripe\Customer::create([
                    'email' => auth()->user()->email,
                    'source' => $request->input('stripeToken'),
                ]);

                // sauvegarde du id customer
                if ($customer->id !== null)
                {
                    auth()->user()->stripe_customer = $customer->id;
                    auth()->user()->save();
                }

            } else {

                // maj du customer
                $customer = \Stripe\Customer::update(
                    auth()->user()->stripe_customer,[
                    'email' => auth()->user()->email,
                    'source' => $request->input('stripeToken'),
                ]);
            }

            // demande abonnement
            $subscription = \Stripe\Subscription::create([
                'customer' => auth()->user()->stripe_customer,
                'items' => [['plan' => 'plan_GNVGzjI1ItPdgW']],

            ]);


            $plan = \Stripe\Plan::retrieve(
                $request->plan
            );



            $payment = new Payment;
            $payment->payment_id = $subscription->id;
            $payment->payer_email = auth()->user()->email;
            $payment->plan = $plan->nickname;
            $payment->amount = $plan->amount/100;
            $payment->currency = env('STRIPE_CURRENCY');
            $payment->payment_status = $subscription->status;
            $payment->save();


            var_dump($subscription);

        }





/*
        if ($request->input('stripeToken')) {



            if ($response->isSuccessful()) {
                // payment was successful: insert transaction data into the database
                $arr_payment_data = $response->getData();

                $isPaymentExist = Payment::where('payment_id', $arr_payment_data['id'])->first();

                if(!$isPaymentExist)
                {

                }

                return "Payment is successful. Your payment id is: ". $arr_payment_data['id'];
            } else {
                // payment failed: display message to customer
                return $response->getMessage();
            }
        }
*/
    }

}

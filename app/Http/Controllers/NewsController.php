<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\News;
use Illuminate\Support\Str;

use App\Repositories\NewsRepository;

use App\Http\Requests\NewsStore;
use App\Models\Upload;



class NewsController extends Controller
{

    /**
     * News repository.
     *
     * @var App\Repositories\NewsRepository
     */
    protected $newsRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(NewsRepository $newsRepository)
    {
        $this->newsRepository = $newsRepository;
    }


    /**
     * affiche la liste des articles
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = $this->newsRepository->last(8);

       return view('news.list', compact('news'));
    }

    /**
     * affiche la page pour creer un article
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if(!$request->session()->has('index')) {
            $request->session()->put('index', Str::random(30));
        }

        $categories['news'] = 'Article';
        $categories['coverage'] = 'Reportage';

        return view('news.create', compact('categories'));
    }

    /**
     * enregistre la nouvelle annonce
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news = $this->newsRepository->create([
            'title' => $request->title,
            'texte' => $request->texte,
            'category' => $request->category,
            'active' => true
        ]);

        if($request->session()->has('index')) {
            $index = $request->session()->get('index');
            Upload::whereIndex($index)->update(['news_id' => $news->id, 'index' => 0]);
        }

        $news = $this->newsRepository->last(8);
        return view('news.list', compact('news'));
    }

    /**
     * affiche un article
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //$this->authorize('show', $ad);
        $photos = $this->newsRepository->getPhotos($news);
        return view('news.show', compact('news', 'photos'));
    }

    /**
     * affiche la page pour modifier un article
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {

        //$this->authorize('show', $ad);
        $photos = $this->newsRepository->getPhotos($news);

        $categories['news'] = 'Article';
        $categories['coverage'] = 'Reportage';

        return view('news.edit', compact('news', 'photos', 'categories'));
    }

    /**
     * modifie un article
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {

        //$this->authorize('manage', $news);
        $this->newsRepository->update($news, $request->all());
        $request->session()->flash('status', "L'annonce a bien été modifiée.");
        return back();
    }


    /**
     * supprime un article
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, News $news)
    {

        // $this->authorize('manage', $news);
        $this->newsRepository->delete($news);
        $request->session()->flash('status', "L'article a bien été supprimée.");

        $news = $this->newsRepository->last(8);

        return view('news.list', compact('news'));
    }

}

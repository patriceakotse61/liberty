<style>

    html {
        line-height: 1;
        text-size-adjust: 100%;
        box-sizing: border-box;
        font-size: 62.5%;
    }

    body {
        font-family: 'Roboto Slab', serif;
        font-size: 1.4em;
        font-weight: 400;
        line-height: 1.6;
        color: #222;
    }

    /* Flexbox test */
    .view-content {
        display: flex;
        flex-flow: row wrap; /* -direction -wrap */
        justify-content: flex-start;
        align-items: stretch;
        align-content: flex-start;
    }

    .item {
        margin: 0 10px 5px 0;
        padding: 0 10px 20px 0;
        width: 310px;
        flex: 0 0 0; /* -grow -shrink -basis */
    }

    .item-image {
        width: 310px;
        height: 250px;
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center top;
    }

    /* theme */
    html, body {
        color: #000;
        font-family: "Source Sans Pro", "Arial", "Helvetica", sans-serif;
        font-size: 15px;
        font-weight: normal;
        letter-spacing: 0;
        line-height: 1.4;
    }

    .field-content {
        display: block;
    }

    .item img {
        display: block;
        margin: auto;
        padding-bottom: 3px;
    }

    .date-created {
        color: #666;
        padding: 3px 0;
    }

    .item h3, .item h4 {
        font-family: "Source Sans Pro", "Arial", "Helvetica", sans-serif;
        font-size: 1em;
        font-weight: bold;
        margin: 0;
        padding: 6px 0 3px;
    }

    .item p {
        font-size: 0.867em;
        font-weight: normal;
    }

    p:last-child {
        margin-bottom: 0;
    }

    p {
        margin: 0 0 15px;
    }

    a {
        color: #000;
        text-decoration: none
    }

    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
        font-family: "Source Serif Pro", "Georgia", "Times", serif;
        font-weight: 600;
        line-height: 1.2;
        margin: 30px 0 15px;
    }


</style>

<?php

//find out which feed was selected
$xml = "https://news.google.com/rss/search?num=20&hl=fr&gl=FR&ceid=FR:fr&q=h";
$url = "https://news.google.com/news?ned=fr&topic=h&output=rss";

// ok
$info = array(
    array( "source" => "lemonde", "link" => "https://www.lemonde.fr/rss/une.xml"),
    array( "source" => "franceinfo", "link" => "https://www.francetvinfo.fr/titres.rss"),
    array( "source" => "france24", "link" => "https://www.france24.com/fr/rss"),
    array( "source" => "yahoo", "link" => "https://fr.news.yahoo.com/rss/"),
    array( "source" => "bing", "link" => "https://www.bing.com/news/search?q=%22%22&format=rss"),
);


$nb =rand(0, count($info)-1);

$url = $info[$nb]['link'];
$source = $info[$nb]['source'];


$rss = simplexml_load_file($url, null, LIBXML_NOCDATA);
$namespaces = $rss->getNamespaces(true);



function getAttrib($data, $name)
{
    $attrib = null;
    $json = json_encode($data);
    $array = json_decode($json, TRUE);
    if (isset($array[$name]))
        $attrib = $array[$name];

    if (isset($array['@attributes'][$name]))
        $attrib = $array['@attributes'][$name];

    return $attrib;

}

$i = 0;
$feeds = [];
foreach ($rss->channel->item as $item) {

    // var_dump($item);
    $feeds[$i]['num'] = $i;
    $feeds[$i]['title'] = (string)$item->title;
    $feeds[$i]['link'] = (string)$item->link;
    $feeds[$i]['date'] = getAttrib($item->pubDate, "0");
    $feeds[$i]['source'] = getAttrib($item->source, "0");
    $feeds[$i]['picture'] = getAttrib($item->enclosure, "url");
    $feeds[$i]['description'] = (string)$item->description;
    if (@$item->children($namespaces['media'])->content->attributes()->url != null)
        $feeds[$i]['picture'] = $item->children($namespaces['media'])->content->attributes()->url;
    if (@isset($item->children($namespaces['News'])->Image))
        $feeds[$i]['picture'] = $item->children($namespaces['News'])->Image;

    $i++;
}
?>


<div class="view view-dernieres-actus">
    <div class="view-content">
        <?php foreach ($feeds as $feed) { ?>
        <div class="item">
            <div class="views-field views-field-title">
                <span class="field-content">
                    <a href="<?php echo $feed['link'] ?>" target="tab">
                        <div class="item-image" style="background-image:url(<?php echo $feed['picture'] ?>)"></div>
                        <div class="date-created"><?php echo date("d-m-Y h:i:s", strtotime($feed['date'])) ." ".$source?></div>
                        <h3><?php echo $feed['title'] ?></h3>
                        <p><?php echo $feed['description'] ?></p>
                    </a>
                </span>
            </div>
        </div>
        <?php
           if ($feed['num'] >= 7) break;
        }
        ?>


    </div>
</div>

